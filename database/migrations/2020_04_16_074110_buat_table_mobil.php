<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTableMobil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobil', function (Blueprint $table) {
            $table->string('mobilid');
            $table->string('mobilfoto');
            $table->string('mobilnopol');
            $table->string('mobilnama');
            $table->string('mobilmerk');
            $table->integer('mobiltahun');
            $table->integer('mobilkapasitas');
            $table->string('mobilbensin');
            $table->string('mobilwarna');
            $table->string('mobilstatus');
            $table->string('mobildeskripsi');
            $table->string('mobilkondisi');
            $table->integer('mobilharga');
            $table->timestamps();

            $table->primary('mobilid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
