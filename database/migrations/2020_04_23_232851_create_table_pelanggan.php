<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePelanggan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelanggan', function (Blueprint $table) {
            $table->string('pelid',6);
            $table->string('pelnik',16);
            $table->string('pelnama',100);
            $table->string('peljk',20);
            $table->string('pelalamat',100);
            $table->String('peltelp',13);
            $table->string('pelemail');
            $table->string('pelpass',20);
            $table->string('pelfoto');
            $table->timestamps();

            $table->primary('pelid');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelanggan');
    }
}
