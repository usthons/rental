<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/dashboard', function () {
//     return view('dashboard');
// });
// Route::get('/about', function () {
//     return view('about');
// });

Route::get('/', 'PageController@home'); 
Route::get('/dashboard', 'PageController@dash'); 

//merk
//anggota
Route::get('/merk', 'MerkController@index'); 
Route::get('/merk.create', 'MerkController@create'); 
Route::post('/merk', 'MerkController@store'); 
Route::get('/merk:{merkid}', 'MerkController@destroy'); 
Route::get('/editmerk/{merkid}', 'MerkController@edit'); 
Route::patch('/editmerk/{merkid}', 'MerkController@update'); 

//Mobil

Route::get('/mobil', 'MobilController@index');
Route::get('/mobil.create', 'MobilController@create'); 
Route::post('/mobil', 'MobilController@store'); 
Route::get('/mobil:{mobilid}', 'MobilController@destroy'); 
Route::get('/editmobil/{merkid}', 'MobilController@edit'); 
Route::patch('/editmobil/{mobilid}', 'MobilController@update');  

// pelanggan
Route::get('/pelanggan', 'PelangganController@index');
Route::get('/pelanggan.create', 'PelangganController@create'); 
Route::post('/pelanggan', 'PelangganController@store'); 
Route::get('/pelanggan:{pelid}', 'PelangganController@destroy'); 
Route::get('/editpelanggan/{pelid}', 'PelangganController@edit'); 
Route::patch('/editpelanggan/{pelid}', 'PelangganController@update'); 

