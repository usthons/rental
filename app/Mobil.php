<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobil extends Model
{
    protected $table = 'mobil';

    protected $fillable = 
    [
        'mobilid',
        'mobilnopol',
        'mobilnama',
        'mobilmerk',
        'mobiltahun',
        'mobilkapasitas',
        'mobilbensin',
        'mobilwarna',
        'mobilfoto',
        'mobilstatus',
        'mobildeskripsi',
        'mobilkondisi',
        'mobilharga'
    ];
}
