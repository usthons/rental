<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Merk;
class MerkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $merk = Merk::all();
        return view('merk.index', compact('merk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('merk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       Merk::create([
           'merkid' => $request->merkid,
           'merknama' => $request->merknama
       ]);

       return redirect('/merk')->with('status','Data Berhasil di Simpan');
        // $msg = [
        //     'required' => ':attribute tidak boleh dikosongi!!'
        // ];
        // $this->validate($request,[
        //     'id' => 'required',
        //     'nama' => 'required|alpha'
        // ],$msg);

        // Merk::create($request->all());
        // return redirect('/merk')->with('status','Data Berhasil di Simpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $merk = Merk:: where('merkid',$id)->first();
        return view('merk.edit', compact('merk') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Merk::where('merkid',$id)->update([
            'merkid'=> $request->merkid,
            'merknama'=> $request->merknama

        ]);
        return redirect()->to('/merk')->with('status','Data Berhasil di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $merk = Merk::where('merkid',$id)->delete();
        return redirect()->to('/merk')->with('status','Data Berhasil di Hapus');

    }
}
