<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mobil;
class MobilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mobil = Mobil::all();
        return view('mobil.index', compact('mobil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mobil.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('image')){
            $resorce  = $request->file('image');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/foto", $name);
            
        Mobil::create([
            'mobilid' => $request->mobilid,
            'mobilfoto' => $name,
            'mobilnopol' => $request->mobilnopol,
            'mobilnama' => $request->mobilnama,
            'mobilmerk' => $request->mobilmerk,
            'mobiltahun' => $request->mobiltahun,
            'mobilkapasitas' => $request->mobilkapasitas,
            'mobilbensin' => $request->mobilbensin,
            'mobilwarna' => $request->mobilwarna,
            'mobilstatus' => $request->mobilstatus,
            'mobildeskripsi' => $request->mobildeskripsi,
            'mobilkondisi' => $request->mobilkondisi,
            'mobilharga' => $request->mobilharga
        ]);
 
        return redirect('/mobil')->with('status','Data Berhasil di Simpan');
        }
        else {
            echo 'gagal';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mobil = Mobil:: where('mobilid',$id)->first();
        return view('mobil.edit', compact('mobil') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Mobil::where('mobilid',$id)->update([
            'mobilid' => $request->mobilid,
            'mobilnopol' => $request->mobilnopol,
            'mobilnama' => $request->mobilnama,
            'mobilmerk' => $request->mobilmerk,
            'mobiltahun' => $request->mobiltahun,
            'mobilkapasitas' => $request->mobilkapasitas,
            'mobilbensin' => $request->mobilbensin,
            'mobilwarna' => $request->mobilwarna,
            'mobilstatus' => $request->mobilstatus,
            'mobildeskripsi' => $request->mobildeskripsi,
            'mobilkondisi' => $request->mobilkondisi,
            'mobilharga' => $request->mobilharga

        ]);
        return redirect()->to('/mobil')->with('status','Data Berhasil di Update');
    }
      

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mobil = Mobil::where('mobilid',$id)->delete();
        return redirect()->to('/mobil')->with('status','Data Berhasil di Hapus');
    }
}
