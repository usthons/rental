<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $table = 'pelanggan';

    protected $fillable = 
    [
        'pelid',
        'pelnik',
        'pelnama',
        'peljk',
        'pelalamat',
        'peltelp',
        'pelemail',
        'pelpass',
        'pelfoto'
    ];
}
