@extends('layout.master')

@section('content')
<div class="col-lg-6 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Daftar Merk</h4>
        <p class="card-description">
            <a href="merk.create" class="mdi mdi-plus-circle-outline badge badge-primary">  Tambah Merk</a>
            @if(session('status'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            {{session('status')}} 
            </div>
            @endif
        </p>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>ID Merk</th>
                <th>Nama Merk</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($merk as $m)
                <tr>
                    <td>{{$m->merkid}}</td>
                    <td>{{$m->merknama}}</td>
                    <td>
                        <a href="/editmerk/{{$m->merkid}}" class="mdi mdi-lead-pencil badge badge-warning">Edit</a>
                        <a href="/merk:{{$m->merkid}}" class="mdi mdi-delete badge badge-danger">Delete</a>
                    </td>
                </tr>  
                @endforeach
             
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

@endsection