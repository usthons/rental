@extends('layout.master')

@section('content')
<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Form Tambah Data Merk </h4>
        <p class="card-description">
            @if(session('status'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            {{session('status')}} 
            </div>
            @endif
        </p>
        <form class="forms-sample" method="POST" action="/merk">
        @csrf
          <div class="form-group row">
            <label for="merkid" class="col-sm-3 col-form-label" for="merkid">ID Merk</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="merkid" name="merkid" placeholder="ID Merk" >
            </div>
          </div>
          <div class="form-group row">
            <label for="merknama" class="col-sm-3 col-form-label" for="merknama">Nama Merk</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="merknama" name="merknama" placeholder="Nama Merk" >
            </div>
          </div>
          <button type="submit" class="badge badge-primary mdi mdi-content-save">  Simpan</button>
        </form>
      </div>
    </div>
  </div>

@endsection