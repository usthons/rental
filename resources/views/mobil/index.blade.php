@extends('layout.master')

@section('content')
<div class="">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Hoverable Table</h4>
        <p class="card-description">
            <a href="mobil.create" class="mdi mdi-plus-circle-outline badge badge-primary">  Tambah Mobil</a>
            @if(session('status'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            {{session('status')}} 
            </div>
            @endif
        </p>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Id Mobil</th>
                <th>Foto Mobil</th>
                <th>Nopol Mobil</th>
                <th>Nama Mobil</th>
                <th>Merek Mobil</th>
                <th>Tahun Mobil</th>
                <th>Kapasitas Mobil</th>
                <th>Bensin Mobil</th>
                <th>Warna Mobil</th>
                <th>Status Mobil</th>
                <th>Descripsi Mobil</th>
                <th>Kondisi Mobil</th>
                <th>Harga Mobil</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($mobil as $mb)
                <tr>
                    <td>{{$mb->mobilid}}</td>
                    <td><img src="{{ asset('foto/'.$mb->mobilfoto) }}" /></td>
                    <td>{{$mb->mobilnopol}}</td>
                    <td>{{$mb->mobilnama}}</td>
                    <td>{{$mb->mobilmerk}}</td>
                    <td>{{$mb->mobiltahun}}</td>
                    <td>{{$mb->mobilkapasitas}}</td>
                    <td>{{$mb->mobilbensin}}</td>
                    <td>{{$mb->mobilwarna}}</td>
                    <td>{{$mb->mobilstatus}}</td>
                    <td>{{$mb->mobildeskripsi}}</td>
                    <td>{{$mb->mobilkondisi}}</td>
                    <td>{{$mb->mobilharga}}</td>
                    <td>
                        <a href="/editmobil/{{$mb->mobilid}}" class="mdi mdi-lead-pencil badge badge-warning">Edit</a>
                        <a href="/mobil:{{$mb->mobilid}}" class="mdi mdi-delete badge badge-danger">Delete</a>
                    </td>
                </tr>  
                @endforeach
             
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

@endsection