@extends('layout.master')

@section('content')
<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Form Tambah Data Mobil </h4>
        <p class="card-description">
            @if(session('status'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            {{session('status')}} 
            </div>
            @endif
        </p>
        <form class="forms-sample" method="POST" action="/mobil" enctype="multipart/form-data" >
        @csrf
          <div class="form-group row">
            <label for="mobilid" class="col-sm-3 col-form-label" for="mobilid">ID Mobil</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="mobilid" name="mobilid" placeholder="ID Mobil" >
            </div>
          </div>
          <div class="form-group row">
            <label for="mobilnopol" class="col-sm-3 col-form-label" for="mobilnopol">Nopol Mobil</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="mobilnopol" name="mobilnopol" placeholder="Nopol Mobil" >
            </div>
          </div>
          <div class="form-group row">
            <label for="mobilnama" class="col-sm-3 col-form-label" for="mobilnama">Nama Mobil</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="mobilnama" name="mobilnama" placeholder="Nama Mobil" >
            </div>
          </div>
          <div class="form-group row">
            <label for="mobilmerk" class="col-sm-3 col-form-label" for="mobilmerk">Merk Mobil</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="mobilmerk" name="mobilmerk" placeholder="Merk Mobil" >
            </div>
          </div>
          <div class="form-group row">
            <label for="mobiltahun" class="col-sm-3 col-form-label" for="mobiltahun">Tahun Mobil</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="mobiltahun" name="mobiltahun" placeholder="Tahun Mobil" >
            </div>
          </div>
          <div class="form-group row">
            <label for="mobilkapasitas" class="col-sm-3 col-form-label" for="mobilkapasitas">Kapasitas Mobil</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="mobilkapasitas" name="mobilkapasitas" placeholder="Kapasitas Mobil" >
            </div>
          </div>
          <div class="form-group row">
            <label for="mobilbensin" class="col-sm-3 col-form-label" for="mobilbensin">Bensin Mobil</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="mobilbensin" name="mobilbensin" placeholder="Bensin Mobil" >
            </div>
          </div>
          <div class="form-group row">
            <label for="mobilwarna" class="col-sm-3 col-form-label" for="mobilwarna">Warna Mobil</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="mobilwarna" name="mobilwarna" placeholder="Warna Mobil" >
            </div>
          </div>
          <div class="form-group row">
            <label for="mobilstatus" class="col-sm-3 col-form-label" for="mobilstatus">Status Mobil</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="mobilstatus" name="mobilstatus" placeholder="Status Mobil" >
            </div>
          </div>
          <div class="form-group row">
            <label for="mobildeskripsi" class="col-sm-3 col-form-label" for="mobildeskripsi">Deskripsi Mobil</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="mobildeskripsi" name="mobildeskripsi" placeholder="Deskripsi Mobil" >
            </div>
          </div>
          <div class="form-group row">
            <label for="mobilkondisi" class="col-sm-3 col-form-label" for="mobilkondisi">Kondisi Mobil</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="mobilkondisi" name="mobilkondisi" placeholder="Kondisi Mobil" >
            </div>
          </div>
          <div class="form-group row">
            <label for="mobilharga" class="col-sm-3 col-form-label" for="mobilharga">Harga Mobil</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="mobilharga" name="mobilharga" placeholder="Harga Mobil" >
            </div>
          </div>
          <input type="file" name="image" id="file" onchange="return fileValidation()"/>
          <div id="imagePreview" style="width : 70px;"></div>
          <button type="submit" class="badge badge-primary mdi mdi-content-save">  Simpan</button>
        </form>
      </div>
    </div>
  </div>

@endsection