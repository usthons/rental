@extends('layout.master')

@section('content')
<div class="">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Daftar Pelanggan</h4>
      <p class="card-description">
        <a href="pelanggan.create" class="mdi mdi-plus-circle-outline badge badge-primary"> Tambah Pelanggan</a>
            @if(session('status'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            {{session('status')}} 
            </div>
            @endif
      </p>
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>ID Pelanggan</th>
              <th>NIK</th>
              <th>Nama</th>
              <th>Jenis Kelamin</th>
              <th>Alamat</th>
              <th>No. Telp</th>
              <th>Email</th>
              <th>Password</th>
              <th>Opsi</th>    
            </tr>
          </thead>
          <tbody>
            @foreach ($pelanggan as $p)
            <tr>
              <td>{{$p->pelid}}</td>
              <td>{{$p->pelnik}}</td>
              <td>{{$p->pelnama}}</td>
              <td>{{$p->peljk}}</td>
              <td>{{$p->pelalamat}}</td>
              <td>{{$p->peltelp}}</td>
              <td>{{$p->pelemail}}</td>
              <td>{{$p->pelpass}}</td>
              <td>
                  <a href="/editpelanggan/{{$p->pelid}}" class="mdi mdi-lead-pencil badge badge-warning">Edit</a>
                  <a href="/pelanggan:{{$p->pelid}}" class="mdi mdi-delete badge badge-danger">Delete</a>
              </td>    
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection