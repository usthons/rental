@extends('layout.master')

@section('content')
<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Form Tambah Data Pelanggan </h4>
        <p class="card-description">
            @if(session('status'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            {{session('status')}} 
            </div>
            @endif
        </p>
        <form class="forms-sample" method="POST" action="/pelanggan">
        @csrf
          <div class="form-group row">
            <label for="pelid" class="col-sm-3 col-form-label" for="pelid">ID Pelanggan</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="pelid" name="pelid" placeholder="ID Pelanggan" >
            </div>
          </div>
          <div class="form-group row">
            <label for="pelnik" class="col-sm-3 col-form-label" for="pelnik">NIK</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="pelnik" name="pelnik" placeholder="NIK Pelanggan" >
            </div>
          </div>
          <div class="form-group row">
            <label for="pelnama" class="col-sm-3 col-form-label" for="pelnama">Nama</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="pelnama" name="pelnama" placeholder="Nama Pelanggan" >
            </div>
          </div>
          <div class="form-group row">
            <label for="peljk" class="col-sm-3 col-form-label" for="peljk">Jenis Kelamin</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="peljk" name="peljk" placeholder="Jenis Kelamin Pelanggan" >
            </div>
          </div>
          <div class="form-group row">
            <label for="pelalamat" class="col-sm-3 col-form-label" for="pelalamat">Alamat</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="pelalamat" name="pelalamat" placeholder="Alamat Pelanggan" >
            </div>
          </div>
          <div class="form-group row">
            <label for="peltelp" class="col-sm-3 col-form-label" for="peltelp">No Telephone</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="peltelp" name="peltelp" placeholder="No.Telp Pelanggan" >
            </div>
          </div>
          <div class="form-group row">
            <label for="pelemail" class="col-sm-3 col-form-label" for="pelemail">Email</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="pelemail" name="pelemail" placeholder="Email Pelanggan" >
            </div>
          </div>
          <div class="form-group row">
            <label for="pelpass" class="col-sm-3 col-form-label" for="pelpass">Password Pelanggan</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" id="pelpass" name="pelpass" placeholder="Password Pelanggan" >
            </div>
          </div>
          
          <button type="submit" class="badge badge-primary mdi mdi-content-save">  Simpan</button>
        </form>
      </div>
    </div>
  </div>

@endsection