<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="{{ url('dashboard') }}">
          <i class="mdi mdi-home menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#mobil" aria-expanded="false" aria-controls="ui-basic">
          <i class="mdi mdi-account menu-icon"></i>
          <span class="menu-title">Data Mobil</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="mobil">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{ url('mobil')}}">Mobil Master</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{ url('merk')}}">Data Merk </a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#user" aria-expanded="false" aria-controls="ui-basic">
          <i class="mdi mdi-file menu-icon"></i>
          <span class="menu-title">Data User</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="user">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="#">Admin</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ url('pelanggan')}}">Pelanggan</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#trx" aria-expanded="false" aria-controls="ui-basic">
          <i class="mdi mdi-book menu-icon"></i>
          <span class="menu-title">Data Transaksi</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="trx">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Lihat Data</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#r" aria-expanded="false" aria-controls="ui-basic">
          <i class="mdi mdi-book menu-icon"></i>
          <span class="menu-title">Report</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="r">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Lihat Data</a></li>
          </ul>
        </div>
      </li>
    </ul>
  </nav>